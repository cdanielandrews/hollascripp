{
	var Holla = function() {
		var positionX = Math.floor(Math.random()*window.innerWidth);
		var positionY = Math.floor(Math.random()*window.innerHeight);
		
		if(positionX > 100) {
			positionX-= 100;
		}
		if (positionY > 300) {
			positionY-= 300;
		}
		
		var newHollaHeader = document.createElement('h3');
		newHollaHeader.appendChild(document.createTextNode('Holla!'));
		var newHollaClose = document.createElement('a');
		newHollaClose.appendChild(document.createTextNode('YEAH!'));
		newHollaClose.href = "#";
		
		var newHolla = document.createElement('div');
		//newHolla.appendChild(newHollaHeader);
		//newHolla.appendChild(newHollaClose);
		newHolla.className = 'holla';
		newHolla.style.top = positionY + 'px';
		newHolla.style.left = positionX + 'px';
		
		newHolla.onclick = function() {
			document.body.removeChild(newHolla);
			setTimeout('Holla();', Math.floor(Math.random()*3000));
		};
		
		document.body.appendChild(newHolla);
		
		setTimeout('Holla();', Math.floor(Math.random()*3000));
	}
	
	var HollaLoad = function(event) {
		setTimeout('Holla();', Math.floor(Math.random()*2000));
	}
	
	if (window.addEventListener) { // Mozilla, Netscape, Firefox
	    window.addEventListener('load', HollaLoad, false);
	} else if (window.attachEvent) { // IE
	    window.attachEvent('onload', HollaLoad);
	}
}