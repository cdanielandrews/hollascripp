#Prepare to be amazed.
Just open the example.html file to see HollaScripp in action!

Adding HollaScripp to your project is easy.

	<script src="lib/hollascripp.js"></script>
	<link rel="stylesheet" type="text/css" href="lib/hollascripp.css">